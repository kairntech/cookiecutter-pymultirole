# Sherpa multirole plugin package template

This is a [Cookiecutter](https://cookiecutter.readthedocs.io/en/latest/) template for
Python 3.x packages, which conforms to my (@jbweston) preferred tooling/organization.

This python package template uses:

+ [Flit](https://flit.readthedocs.io/en/latest/) for building the package
+ [pre-commit](https://pre-commit.com/) for managing pre-commit hooks
+ [bump2version](https://github.com/c4urself/bump2version) for version bumping
+ [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) for changelog formatting
+ [tox](https://tox.readthedocs.io/en/latest/) for test, coverage and documentation building automation
+ [pytest](https://pytest.org/en/latest/) for testing, using:
  + [hypothesis](https://hypothesis.works/) for property-based tests
  + [flake8](http://flake8.pycqa.org/en/latest/) for linting
  + [black](https://black.readthedocs.io/en/stable/) for code formatting
  + [mypy](http://mypy-lang.org/) for static type checking
+ [coverage](https://coverage.readthedocs.io/en/stable/) for code coverage reporting
+ [GitHub Actions](https://github.com/features/actions) for running tests after each push to GitHub
+ [Sphinx](https://www.sphinx-doc.org/en/master/) for documentation building

## Set up environment (method 1)
### Install python3 (minimun 3.8) , pip and venv
```
sudo apt install python3
sudo apt install python3-pip
sudo apt install python3-venv
```
### Create a virtual env
```
mkdir venv
python3 -m venv venv
source venv/bin/activate
```

## Set up environment (method 2)
### Create a virtual env using conda
You need to have [conda](https://conda.io/projects/conda/en/latest/user-guide/install/index.html) `installed`
```
conda create --name pymultirole python=3.8
conda activate pymultirole
```

## Set up environment (method 3)
### Create a virtual env using pyenv
You need to have [pyenv](https://realpython.com/intro-to-pyenv/) `installed`
- check that you have a pyton 3.8.x already installed
```
pyenv versions
```
- if it is not the case install it
```
pyenv install -v 3.8.3
```
- create a virtual env and activate it
```
pyenv virtualenv 3.8.3 pymultirole
pyenv activate pymultirole
```

## Developing
### Pre-requesites
You will need to install `cookiecutter`, `flit` (for building the package) and `tox` (for orchestrating testing and documentation building):
```
python3 -m pip install click==7.1.2
python3 -m pip install cookiecutter
python3 -m pip install flit tox
```
Clone the repository:
```
git clone git@bitbucket.org:kairntech/cookiecutter-pymultirole.git
```

## Create a new plugin from the template
### Call the cookiecutter CLI
```
cookiecutter -f cookiecutter-pymultirole
```
### Answer all the questions
```
Select plugin_type:
1 - converter
2 - formatter
3 - annotator
Choose from 1, 2, 3 [1]: 1
plugin_name [my_plugin]: my_great_converter
package_name [pyconverters_my_great_converter]: 
dist_name [pyconverters-my_great_converter]: 
package_description [My new converter]: My new great converter
package_keywords []: 
author_name [Olivier Terrier]: 
author_email [olivier.terrier@kairntech.com]: 
author_github [oterrier]: 
package_repo [oterrier/pyconverters_my_great_converter]: 
python [default]: 
```
### Build the plugin
```
cd pyconverters_my_great_converter
flit install
```
### Running the test suite
You can run the full test suite against all supported versions of Python with:
```
tox
```

### Building the documentation
You can build the HTML documentation with:
```
tox -e docs
```

The built documentation is available at `docs/_build/index.html.

### Install the package on pypi
```
flit publish
```