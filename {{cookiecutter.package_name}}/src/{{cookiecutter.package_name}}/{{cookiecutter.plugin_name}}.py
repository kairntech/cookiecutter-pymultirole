{%- if cookiecutter.plugin_type == "converter" %}
from typing import Type, List, cast
from starlette.datastructures import UploadFile
{%- elif cookiecutter.plugin_type == "formatter" %}
from typing import Type, cast
from starlette.responses import Response, JSONResponse
{%- elif cookiecutter.plugin_type in ["annotator", "processor", "segmenter"] %}
from typing import Type, List, cast
{%- endif %}
from pydantic import BaseModel, Field
from pymultirole_plugins.v1.{{cookiecutter.plugin_type}} import {{cookiecutter.plugin_type|capitalize}}Parameters, {{cookiecutter.plugin_type|capitalize}}Base
from pymultirole_plugins.v1.schema import Document


class {{cookiecutter.plugin_name|capitalize}}Parameters({{cookiecutter.plugin_type|capitalize}}Parameters):
    foo: str = Field("foo", description="Foo")
    bar: float = Field(0.123456789, description="Bar")


class {{cookiecutter.plugin_name|capitalize}}{{cookiecutter.plugin_type|capitalize}}({{cookiecutter.plugin_type|capitalize}}Base):
    """{{cookiecutter.plugin_name|capitalize}} {{cookiecutter.plugin_type}} .
    """

{%- if cookiecutter.plugin_type == "converter" %}

    def convert(self, source: UploadFile, parameters: ConverterParameters) \
            -> List[Document]:
        params: {{cookiecutter.plugin_name|capitalize}}Parameters =\
            cast({{cookiecutter.plugin_name|capitalize}}Parameters, parameters)
        return [Document(text="Test {{cookiecutter.plugin_name}}", metadata=params.dict())]
{%- elif cookiecutter.plugin_type == "formatter" %}
    def format(self, document: Document, parameters: FormatterParameters) \
            -> Response:
        params: {{cookiecutter.plugin_name|capitalize}}Parameters =\
            cast({{cookiecutter.plugin_name|capitalize}}Parameters, parameters)
        output = {
            "text": document.text,
            "foo": params.foo,
            "bar": params.bar
        }
        return JSONResponse(content=output)
{%- elif cookiecutter.plugin_type == "annotator" %}
    def annotate(self, documents: List[Document], parameters: AnnotatorParameters) \
            -> List[Document]:
        params: {{cookiecutter.plugin_name | capitalize}}Parameters =\
            cast({{cookiecutter.plugin_name | capitalize}}Parameters, parameters)
        documents[0].annotations = [{'start': 0, 'end': 1, 'labelName': params.foo}]
        return documents
{%- elif cookiecutter.plugin_type == "processor" %}
    def process(self, documents: List[Document], parameters: ProcessorParameters) \
            -> List[Document]:
        params: {{cookiecutter.plugin_name | capitalize}}Parameters =\
            cast({{cookiecutter.plugin_name | capitalize}}Parameters, parameters)
        documents[0].annotations = [{'start': 0, 'end': 1, 'labelName': params.foo}]
        return documents
{%- elif cookiecutter.plugin_type == "segmenter" %}
    def segment(self, documents: List[Document], parameters: SegmenterParameters) \
            -> List[Document]:
        params: {{cookiecutter.plugin_name | capitalize}}Parameters =\
            cast({{cookiecutter.plugin_name | capitalize}}Parameters, parameters)
        documents[0].sentences = [{'start': 0, 'end': 1}]
        documents[0].boundaries = { params.foo: [{'name': params.bar, 'start': 0, 'end': 1}]}
        return documents
{%- endif %}

    @classmethod
    def get_model(cls) -> Type[BaseModel]:
        return {{cookiecutter.plugin_name|capitalize}}Parameters
