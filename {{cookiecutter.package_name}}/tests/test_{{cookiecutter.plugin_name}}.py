{%- if cookiecutter.plugin_type == "converter" %}
from pathlib import Path
from typing import List
from starlette.datastructures import UploadFile
{%- elif cookiecutter.plugin_type == "formatter" %}
from starlette.responses import Response
{%- elif cookiecutter.plugin_type in ["annotator", "processor", "segmenter"] %}
from typing import List
{%- endif %}
from {{cookiecutter.package_name}}.{{cookiecutter.plugin_name}} import {{cookiecutter.plugin_name|capitalize}}{{cookiecutter.plugin_type|capitalize}}, {{cookiecutter.plugin_name|capitalize}}Parameters
from pymultirole_plugins.v1.schema import Document


def test_{{cookiecutter.plugin_name}}():
    model = {{cookiecutter.plugin_name|capitalize}}{{cookiecutter.plugin_type|capitalize}}.get_model()
    model_class = model.construct().__class__
    assert model_class == {{cookiecutter.plugin_name|capitalize}}Parameters
    {{cookiecutter.plugin_type}} = {{cookiecutter.plugin_name|capitalize}}{{cookiecutter.plugin_type|capitalize}}()
    parameters = {{cookiecutter.plugin_name|capitalize}}Parameters()
{%- if cookiecutter.plugin_type == "converter" %}
    testdir = Path(__file__).parent
    source = Path(testdir, 'data/test.txt')
    with source.open("r") as fin:
        docs : List[Document] = converter.convert(UploadFile(source.name, fin), parameters)
        assert len(docs) == 1
{%- elif cookiecutter.plugin_type == "formatter" %}
    resp: Response = formatter.format(Document(text="Test {{cookiecutter.plugin_name}}", metadata=parameters.dict()), parameters)
    assert resp.status_code == 200
    assert resp.media_type == "application/json"
{%- elif cookiecutter.plugin_type == "annotator" %}
    docs: List[Document] = annotator.annotate([Document(text="Test {{cookiecutter.plugin_name}}", metadata=parameters.dict())], parameters)
    assert len(docs[0].annotations) > 0
{%- elif cookiecutter.plugin_type == "processor" %}
    docs: List[Document] = processor.process([Document(text="Test {{cookiecutter.plugin_name}}", metadata=parameters.dict())], parameters)
    assert len(docs[0].annotations) > 0
{%- elif cookiecutter.plugin_type == "segmenter" %}
    docs: List[Document] = segmenter.segment([Document(text="Test {{cookiecutter.plugin_name}}", metadata=parameters.dict())], parameters)
    assert len(docs[0].sentences) > 0
    assert len(docs[0].boundaries) > 0
{%- endif %}
